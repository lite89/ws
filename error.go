package ws

import (
	"fmt"
	"net/http"
)

// Errors.
var (
	ErrNotModified           = NewError(http.StatusNotModified)
	ErrBadRequest            = NewError(http.StatusBadRequest)
	ErrUnauthorized          = NewError(http.StatusUnauthorized)
	ErrForbidden             = NewError(http.StatusForbidden)
	ErrNotFound              = NewError(http.StatusNotFound)
	ErrMethodNotAllowed      = NewError(http.StatusMethodNotAllowed)
	ErrRequestEntityTooLarge = NewError(http.StatusRequestEntityTooLarge)
	ErrTooManyRequests       = NewError(http.StatusTooManyRequests)
)

// Error represents a custom error.
type Error struct {
	code int
	text string
}

// NewError creates a new custom error by http code.
func NewError(code int, vs ...any) *Error {
	e := Error{code: code}
	if len(vs) > 0 {
		e.text = fmt.Sprint(vs...)
	}
	return &e
}

// Errorf formats according to a format specifier and returns the string as a value that satisfies error.
func Errorf(format string, vs ...any) *Error {
	return &Error{
		code: http.StatusUnprocessableEntity,
		text: fmt.Sprintf(format, vs...),
	}
}

// Code returns the http code.
func (e *Error) Code() int {
	return e.code
}

// Error implements the error interface.
func (e *Error) Error() string {
	if e.text == "" {
		e.text = http.StatusText(e.code)
		if e.text == "" {
			e.text = "Unknown error"
		}
	}
	return e.text
}
