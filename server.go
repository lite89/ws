package ws

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"

	"gitlab.com/erdian718/ioc"
)

// Server represents a web server.
type Server struct {
	Router

	certfile string
	keyfile  string
	server   http.Server
	logger   *log.Logger
	provider *ioc.Provider
}

// New creates a new server.
func New(provider *ioc.Provider) *Server {
	server := new(Server)
	server.Router.children = make(map[string]*Router)
	server.provider = provider.
		AddScoped((func() context.Context)(nil), nil).
		AddScoped((func() http.ResponseWriter)(nil), nil).
		AddScoped((func() *http.Request)(nil), nil)
	return server
}

// Logger optionally specifies the logger.
func (s *Server) Logger(logger *log.Logger) *Server {
	s.logger = logger
	s.server.ErrorLog = logger
	return s
}

// Addr optionally specifies the TCP address for the server to listen on.
func (s *Server) Addr(addr string) *Server {
	s.server.Addr = addr
	return s
}

// TLS optionally specifies the certfile and keyfile for TLS.
func (s *Server) TLS(certfile, keyfile string) *Server {
	s.certfile = certfile
	s.keyfile = keyfile
	return s
}

// Start starts the server.
func (s *Server) Start() error {
	addr := s.server.Addr
	if addr == "" {
		if s.certfile == "" {
			addr = ":http"
		} else {
			addr = ":https"
		}
	}
	s.logger.Printf("ws: starting server %s...", addr)

	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint

		s.logger.Printf("ws: closing server %s...", addr)
		cancel()
		if err := s.server.Shutdown(ctx); err != nil {
			s.logger.Println("ws:", err)
		}
	}()

	s.server.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var err error
		defer func() {
			if x := recover(); x != nil {
				if e, ok := x.(error); ok {
					err = e
				} else {
					err = fmt.Errorf("ws: %v", x)
				}
			}
			s.herror(err, w, r)
		}()
		w.Header().Set("X-Content-Type-Options", "nosniff")

		path := r.URL.Path
		if len(path) > 0 && path[0] == '/' {
			path = path[1:]
		}

		scope, err := s.provider.Create()
		if err != nil {
			return
		}
		defer scope.Release()

		err = scope.Add(ctx, (*context.Context)(nil))
		if err != nil {
			return
		}
		err = scope.Add(w, (*http.ResponseWriter)(nil))
		if err != nil {
			return
		}
		err = scope.Add(r, nil)
		if err != nil {
			return
		}
		err = s.Router.invoke(scope, path)
	})

	var err error
	if s.certfile == "" {
		err = s.server.ListenAndServe()
	} else {
		err = s.server.ListenAndServeTLS(s.certfile, s.keyfile)
	}
	if err != nil {
		s.logger.Println("ws:", err)
	}
	return err
}

func (s *Server) herror(err error, w http.ResponseWriter, r *http.Request) {
	if err == nil {
		return
	}

	var e *Error
	if !errors.As(err, &e) {
		e = NewError(http.StatusInternalServerError)
	}
	if e.code == http.StatusInternalServerError {
		s.logger.Println("ws:", r.Method, r.URL.Path, err)
	}
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.WriteHeader(e.code)
	w.Write([]byte(e.Error()))
}
