package ws

import (
	"strings"

	"gitlab.com/erdian718/ioc"
)

// Router represents a router.
type Router struct {
	children map[string]*Router
}

// Route returns the sub router.
func (r *Router) Route(p string) *Router {
	p = clean(p)
	if p == "" {
		return r
	}

	var key string
	i := strings.IndexByte(p, '/')
	if i < 0 {
		key = p
	} else {
		key = p[:i]
	}

	router := r.children[key]
	if router == nil {
		router = &Router{
			children: make(map[string]*Router),
		}
		r.children[key] = router
	}
	if i < 0 {
		return router
	}
	return router.Route(p[i+1:])
}

func (r *Router) invoke(scope *ioc.Scope, path string) error {
	// TODO: invoke and marshal json.
	return nil
}
