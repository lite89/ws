// Package ws provides a simple web server.
package ws

import "path"

func clean(p string) string {
	return path.Clean("/" + p)[1:]
}
